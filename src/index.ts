import log from '@ajar/marker'
import fs from "fs";
import express,{Request,Response} from "express";

const PORT = 3031;
const HOST="localhost"
const server = express();

server.post("/backup",  (req:Request, res:Response) => {
  console.log('in');
  const wstream = fs.createWriteStream(`backup-${Math.round(Date.now()/1000)}.sql`,'utf-8');
  req.pipe(wstream);
//   req.on('data',(c)=>console.log('chunk',c));
  res.status(200).json("backup success");
})

;(async ()=>{
  //connect to mongo db
  await server.listen(Number(PORT), HOST);
  log.magenta(
    "server backup is live on",
    ` ✨ ⚡ http://${HOST}:${PORT}✨ ⚡`
    );
  })().catch(console.log);
  
  